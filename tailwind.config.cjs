/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'green-light': '#0DB85C',
        light: '#F2F1F7',
        gray:'#B6B6B6',
        'light-gray': "#ADB5C2",
        'little-green': '#EFF9F1',
        'little-blue': '#E6F5FA',
        'little-gray': '#F2F5FA',
        red:'#ED4337',
        disabled: '#ECECEC',
        'my-dark': '#12202D',
        'ghtk-light': '#069255',
        'ghtk-hint': '#808080',
        'regal-gray': '#F1F2F7',
      },
    },
  },
  plugins: [],
}
