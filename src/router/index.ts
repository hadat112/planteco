import { createRouter, createWebHistory } from "vue-router";
import Dashboard from '@/modules/Dashboard/index.vue'


const routes = [
  {
    path: "",
    name: "layout",
    component: () => import ("@/layouts/default/index.vue"),
    children: [
      {
        path: "/dashboard",
        name: "dashboard",
        component: () => import("@/modules/Dashboard/index.vue"),
      },
      {
        path: "/wallets",
        name: "wallets",
        component: () => import("@/modules/Wallets/index.vue"),
      },
      {
        path: "/transactions",
        name: "transactions",
        component: () => import("@/modules/Transactions/index.vue"),
      },{
        path: "/buypec",
        name: "buypec",
        component: () => import("@/modules/BuyPEC/index.vue"),
      },
      {
        path: "/sellpec",
        name: "sellpec",
        component: () => import("@/modules/SellPEC/index.vue"),
      },{
        path: "/eip",
        name: "eip",
        component: () => import("@/modules/EIP/index.vue"),
      },{
        path: "/verification",
        name: "verification",
        component: () => import("@/modules/Verification/index.vue"),
      },{
        path: "/training",
        name: "training",
        component: () => import("@/modules/Training/index.vue"),
      },{
        path: "/reporting",
        name: "reporting",
        component: () => import("@/modules/Reporting/index.vue"),
      },{
        path: "/settings",
        name: "settings",
        component: () => import("@/modules/Setting/index.vue"),
      }
    ]
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});


export default router;