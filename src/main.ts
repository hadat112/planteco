// @ts-ignore
import { createApp } from 'vue'
import App from './App.vue'
import Antd from 'ant-design-vue';


import  router from '@/router'
import "@/assets/global.css"

const app = createApp(App)
app.use(Antd)
app.use(router)
app.mount('#app')
